from distutils.util import execute
from pkgutil import get_data
from readline import insert_text
from turtle import update
from webbrowser import get
import mysql.connector
from datetime import date
import traceback
import time


print("Please connect to the database\n")

server = False

while server != True:

    try:
        my_db = mysql.connector.connect (
        host = input("localhost: "),
        user = input("user: "),
        password = input("password: "),
        database = input("database: ")
        )
        my_cursor = my_db.cursor()
        server = True
    except:
        print("Unable to connect to database.")
        
    


print("###################################################################################################")
print("                                                                                                    \n\n\
                                        .            ....\n\
                                       .cdl.       .,ldxxdl,.\n\
                                     .;dkOx;    .,:lxxkkkkxdc'\n\
                                   .cdkkkkd'   'ldxxxxxdollccc;.\n\
                                 .:dkkkkxo'  'cddddxxdc'.,:ccccc;.\n\
                               .:dxxxxxl,..'codddddolc;,...,cccllc;.\n\
                             .:oxxxxxl,...:ooodolc:::::::,...;clllll:.\n\
                           .;oxxxxdl,...:looool,.  .,:cccc:;...;cloool:.\n\
                         .;odddddl,. .;lllllc,.      .,cccccc;. .;loooooc.\n\
                         ;dddddo;.  'clllll;.          .:lllll:.  .:oddddo'\n\
                         ,oddddo;.  'cccclc,.          .:lllllc.  .:dddddo'\n\
                         .,loooooc'..':ccccc:'.      .,cloool:. .,ldddddl,\n\
                           .,collllc'..,:ccccc;.   .,cooool:...,ldxdxdl,.\n\
                             .,clllll:'..'::::c::::looooo:'..,lddxxdl,.\n\
                               .,cccccc:'..';:clodddddoc'..,ldxxxxl,.\n\
                                 .;cccccc;..'loddddddc' .,ldxxxxd;.\n\
                                   .,:ccccllodddddoc'   ,dxxxxd:.\n\
                                     .;lddxxxxxdl'.     :xxxd:.\n\
                                       .:oxxxdl'.       'od:.\n\
                                         ..''.           ..\n\n\
                                             ")
print("##################################################################################################\n")

time.sleep(2)

print("##################################################################################################\n\
##################################         CALC-U-LATER          #################################\n\
##################################################################################################")


#01 = admin
#02 = Finance
#03 = IT
#04 = Development
#05 = HR
#06 = Management
#07 = Marketing
#08 = Distribution
    
##################################################################################
#########################    Variables/Constants  ################################
##################################################################################

elem1 = "\n##################################################################################################"

errordetail = traceback.format_exc()
sqlerror = "There seems to be an error with the SQL command.\nPlease check your entries or contact your administrator for further maintenance.\n", errordetail
listempty = "Error: There are no entries in this table."
valuefail = "An error with the used values occured.\nContact your administrator for further maintenance."
typefail = "A data type is not compatible", errordetail

##################################################################################
############################    Klass  ###########################################
##################################################################################
class Kost: 
    def enterfix(self, usr):
        #Fixkosten einpflegen
        item = input("Which item shall be entered?: ")
        quant = input("Please enter the calculated amount of the item(s): ")
        cost = input("How high will be the monthly costs for this item/these items?: ")
        
        to_date = convertdate(date.today())
        
        #Check auf erwartete Einnahmen
        try: 
            befehl = "SELECT REVENUE_PLANNED FROM PLAN WHERE MONTH = " + '"' + to_date[:6] + '"'
            my_cursor.execute(befehl)
            planrev = my_cursor.fetchall()
            planrev = float(convert_list(planrev))
        except ValueError:
            print(sqlerror)
        
        #Gesamte Fixkosten holen
        totalfix = GetData.total("FIXED_COSTS")
        
        #Gesamte Budgets holen
        try:
            befehl = "SELECT SUM(AMOUNT) FROM BUDGET WHERE MONTH = " + '"' + to_date[:6] + '"'
            my_cursor.execute(befehl)
            budgets = my_cursor.fetchall()
            budgets = float(convert_list(budgets))
        except ValueError:
            print(sqlerror)
        
        #Wenn die kumulierten Kosten größer sind als die geplanten Einnahmen, gibt's eine Fehlermeldung
        if planrev < totalfix + budgets:
            print("The costs you want to enter exceed to expected companies budget for that month.")
        else:
            try:
                new_entry = "INSERT INTO FIXED_COSTS (ITEM, QUANTITY, COSTS, USER, DATE) VALUES (" + '"' + item + '"' + ', ' + '"' + quant + '"' + ', ' + '"' + cost + '"' + ', ' + '"' + usr + '"' + ', ' + '"' + str(to_date) + '"' + ')'
                my_cursor.execute(new_entry)
                my_db.commit()
                print("Entry has been generated successfully")
            except ValueError:
                print(sqlerror)

    def entervar(self, department, usr):
        #Variable Kosten einpflegen
        item = input("Which item will be bought?\n")
        quant = input("How many?\n")
        cost = input("Please enter the total costs for this item/these items:\n")
        month = input("For which month shall this this purchase be calculated?\n")
        using = input("Please enter a using: \n")
                
        to_date = convertdate(date.today())
        
        
        befehl = "SELECT SUM(COSTS) FROM VARIABLE_COSTS WHERE TARGET_MONTH = " + '"' + month + '"' + " AND DEPARTMENT = " + '"' + get_department(department) + '"'
        my_cursor.execute(befehl)
        totalvar = convert_list(my_cursor.fetchall())
        
        befehl = "SELECT AMOUNT FROM BUDGET WHERE MONTH = " + '"' + month + '"' + " AND DEPARTMENT = " + '"' + get_department(department) + '"'
        my_cursor.execute(befehl)
        depbudget = convert_list(my_cursor.fetchall())
        
        try:
            if float(depbudget) > float(totalvar) + float(cost):
            
                try:
                    new_entry = "INSERT INTO VARIABLE_COSTS (ITEM, QUANTITY, COSTS, DEPARTMENT, USER, DATE, TARGET_MONTH, PURPOSE) VALUES (" + '"' + item + '"' + ', ' + '"' + quant + '"' + ', ' + '"' + cost + '"' + ', ' + '"' + get_department(department) + '"' + ', ' + '"' + usr + '"' + ', ' + '"' + to_date + '"' + ', ' + '"' + month + '"' + ', ' + '"' + using + '"' + ')'
                    my_cursor.execute(new_entry)
                    my_db.commit()
                    print("Entry has been generated successfully")
                except TypeError: 
                    print(typefail)
                except ValueError:
                    print("A budget for that month has not been granted yet.")
                except: 
                    print("An Error with the database occured")
            
            else:
                print("The costs you try to account will exceed the available budget for you department.")
        except ValueError:
            print(valuefail)
        except TypeError:
            print(sqlerror)
        except:
            print("A budget for that month has not been granted yet.")    


class GetData:
    def get_all(table):
        befehl = "SELECT * FROM " +  table
        my_cursor.execute(befehl)
        get_data = my_cursor.fetchall()
        return get_data
    
    def get_allmonth(table, date):
        befehl = "SELECT * FROM " +  table + " WHERE MONTH = " + '"' + date[:6] + '"'
        my_cursor.execute(befehl)
        get_data = my_cursor.fetchall()
        return get_data
    
    def get_rows(table, department, date):
        befehl = "SELECT * FROM " +  table  + " WHERE DEPARTMENT = " + '"' + department + '"' + " AND TARGET_MONTH = " + '"' + date[:6] + '"'
        my_cursor.execute(befehl)
        get_data = my_cursor.fetchall()
        return get_data
    
    def total(table):
        my_cursor.execute("SELECT SUM(COSTS) FROM " + table)
        totalfix = my_cursor.fetchall()
        totalfix = float(convert_list(totalfix))
        return totalfix
    
    def totalmonth(table, month, column):
        my_cursor.execute("SELECT SUM(" + column + ") FROM " + table + " WHERE MONTH = " + month)
        totalfix = my_cursor.fetchall()
        totalfix = float(convert_list(totalfix))
        return totalfix

class Chart():
    def Chart(date):
        Chart.GetBudgets(date)
        print(elem1, "\n")
        Chart.WinLose(date)
        print(elem1, "\n")
        Chart.DevFin(date)
        
    
    def GetBudgets(date):
        print("Distribution of budgets for the date ", date)
        print("\n")       
            
        budgets = GetData.get_allmonth("BUDGET", date[:6])
        revenue = (GetData.get_allmonth("PLAN", date[:6])[0][2],)
        budrevget =[]
        try:
            for row in budgets:    
                row = row + revenue
                budrevget.append(row)
        except ValueError:
            print("No Entries have been found")
        except TypeError:
            print("There seems to be a problem with the database. Please contact your administrator.")
            
        print("{:<10} {:<12} {:<18}".format("BUDGET", "DEPARTMENT", "DISTRIBUTION IN %"))
        for row in budrevget:
            id, amount, dep, month, exp, dist = row
            print("{:<10} {:<12} {:<18}".format(amount, dep, amount/dist*100))
            
        
            
    def WinLose(date):
        print("\nProfits and revenues:\n")
        revenue = convert_list(GetData.get_allmonth("PLAN", date[:6]))
        costsfix = GetData.total("FIXED_COSTS")
        my_cursor.execute("SELECT SUM(COSTS) FROM VARIABLE_COSTS WHERE TARGET_MONTH = " + date[:6])
        costsvar = convert_list(my_cursor.fetchall())
        
        print("Actual revenue this month is: ", revenue[1])
        print("The total costs are estimated at ", str(float(costsfix) + float(costsvar)))
        print(float(revenue[1])-(float(costsfix) + float(costsvar)), " are left of the total revenue of ", revenue[1])
        print("A proft of ", revenue[4], " has been planned for the month ", date[:6], "\n", revenue[3], " has been achieved of it yet.")
        
        
        

    def DevFin(date):
        print("\nCurrent development\n")
        print("{:<12} {:<10} {:<10} {:<10}".format("   " ,str(int(date[:6])-2), str(int(date[:6])-1), date[:6]))
        print("----------------------------------------------")
        revenuethism = convert_zero(GetData.get_allmonth("PLAN", date[:6])[0][1])
        revenuelastm = convert_zero(GetData.get_allmonth("PLAN", str(int(date[:6])-1))[0][1])
        revenuelalam = convert_zero(GetData.get_allmonth("PLAN", str(int(date[:6])-2))[0][1])
        costsfix = convert_zero(GetData.total("FIXED_COSTS"))
        my_cursor.execute("SELECT SUM(COSTS) FROM VARIABLE_COSTS WHERE TARGET_MONTH = " + str(int(date[:6])))
        varthism = convert_zero(convert_list(my_cursor.fetchall()))
        my_cursor.execute("SELECT SUM(COSTS) FROM VARIABLE_COSTS WHERE TARGET_MONTH = " + str(int(date[:6])-1))
        varlastm = convert_zero(convert_list(my_cursor.fetchall()))
        my_cursor.execute("SELECT SUM(COSTS) FROM VARIABLE_COSTS WHERE TARGET_MONTH = " + str(int(date[:6])-2))
        varlalam = convert_zero(convert_list(my_cursor.fetchall()))
        
        
        print("{:<12} {:<10} {:<10} {:<10}".format("REVENUE", str(revenuelalam), str(revenuelastm), str(revenuethism)))
        print("{:<12} {:<10} {:<10} {:<10}".format("VAR. COSTS", str(varlalam), str(varlastm), str(varthism)))
        print("{:<12} {:<10} {:<10} {:<10}".format("TOTAL COSTS", str(float(costsfix) + float(varlalam)), str(float(costsfix) + float(varlastm)), str(float(costsfix) + float(varthism))))
        print("{:<12} {:<10} {:<10} {:<10}".format("PROFIT", str(float(revenuelalam) - (float(costsfix) + float(varlalam))), str(float(revenuelastm) - (float(costsfix) + float(revenuelastm))), str(float(revenuethism) - (float(costsfix) + float(revenuethism)))))
        
        
        
        
        
            
#######################################################################################
############################    Funktionen  ###########################################
#######################################################################################
def convert_zero(variable):
    if variable == None:
        variable = 0
    return variable
        
def convert_list(list):
    try:
        if len(list[0]) > 1:
            new_list = []
            for i in list[0]: 
                new_list.append(i)
            return new_list
        else:
            return list[0][0]
    except TypeError:
        print(listempty)
    except:
        return list

    
def convertdate(dat):
    #Datum vom Datumsformat in einfachen String konvertieren: YYYYMMDD
    datnew = dat.strftime("%Y%m%d")
    return datnew
    
def checkpriv(userpriv, privlist):   
    #Überprüfen ob Berechtigungen des Benutzers mit denen der Funktion übereinstimmen
    if userpriv in privlist:
        return True
    else:
        return False

def get_department(number):
    #Abteilungskürzel in Abteilungsbezeichnung konvertieren
    my_cursor.execute("SELECT NAME FROM DEPARTMENT WHERE PRIVILEGES = " + '"' + number + '"')
    dent = my_cursor.fetchall()
    try:
        return convert_list(dent)
    except:
        print("The entered department doesn't exist. Please check your entries.")

def showdepartments():
    #Alle departments selektieren und anzeigen
    my_cursor.execute("SELECT NAME FROM DEPARTMENT")
    dents = my_cursor.fetchall()

    print("Current departments are:")
    for row in dents:
        print(row[0])
    print(elem1)

def departments():
    #Department eingeben oder verfügbare departments anzeigen lassen
    order = "DEP"
    while order == "DEP":
        order = input("Please type in the targeted department.\nType 'DEP' to see current departments.\n")
        
        if order == "DEP":
            showdepartments()
        else:
            return order
            
    

def enterin(cat, branch):
    
    month = input("Please enter the targeted month (YYYYMM):\n")
    income = input("Please enter the amount?:\n")
    
    my_cursor.execute("SELECT * FROM PLAN WHERE MONTH = " + '"' + month + '"')
    refplan = convert_list(my_cursor.fetchall())
    try:
        if branch == "PLAN":
            if int(month) in refplan:
                if cat == "REV":
                    my_cursor.execute("UPDATE PLAN SET REVENUE_PLANNED = " + '"' + income + '"' + " WHERE MONTH = " + '"' + month + '"')
                    my_db.commit()
                elif cat == "PROF":
                    my_cursor.execute("UPDATE PLAN SET PROFIT_PLANNED = " + '"' + income + '"' + " WHERE MONTH = " + '"' + month + '"')
                    my_db.commit()
                    
            else: 
                if cat == "REV":
                    my_cursor.execute("INSERT INTO PLAN (REVENUE_PLANNED, MONTH) VALUES (" + income + ', ' + month + ')')
                    my_db.commit()
                elif cat == "PROF":
                    my_cursor.execute("INSERT INTO PLAN (PROFIT_PLANNED, MONTH) VALUES (" + income + ', ' + month + ')')
                    my_db.commit()
        
        elif branch == "INCOME":
            if int(month) in refplan:
                my_cursor.execute("UPDATE PLAN SET REVENUE_ACTUAL = " + '"' + str(float(refplan[1]) + float(income)) + '"' + " WHERE MONTH = " + '"' + month + '"')
                my_db.commit()
                
                #Wenn tatsächliche einnahmen + hinzugefügte Einnahmen - alle Fixkosten - alle variablen kosten
                if float(refplan[1]) + float(income) - GetData.total("FIXED_COSTS") - GetData.totalmonth("BUDGET", month, "AMOUNT") >= float(refplan[4]): 
                    
                    print("Congratulations, you have fulfilled the planned win expectations for this month.")
                    my_cursor.execute("UPDATE PLAN SET PROFIT_ACTUAL = " + '"' + str((float(refplan[1]) + float(income))-float(refplan[2])) + '"' + " WHERE MONTH = " + '"' + month + '"')
                    my_db.commit()
            else: 
                my_cursor.execute("INSERT INTO PLAN (REVENUE_ACTUAL, MONTH) VALUES (" + income + ', ' + month + ')')
                my_db.commit()
    except ValueError:
        print(valuefail)
    except TypeError:
        print(sqlerror)
    except:
        print("An error uccoured.")
        
def graphicchart():
    pass
def graphicpie():
    pass
    
def showdata(table, department, datum): 
    befehl = "SELECT AMOUNT FROM BUDGET WHERE DEPARTMENT = " + '"' + department + '"' + " AND MONTH = " + '"' + datum[:6] + '"'
    my_cursor.execute(befehl)
    depbudget = convert_list(my_cursor.fetchall())
        
    if table == "VARIABLE_COSTS":
        befehl = "SELECT AMOUNT FROM BUDGET WHERE DEPARTMENT = " + '"' + department + '"' + " AND MONTH = " + '"' + datum[:6] + '"'
        my_cursor.execute(befehl)
        depbudget = convert_list(my_cursor.fetchall())
        
        print(elem1)
        
        try:
            print("Budget of department", department, ": ", str(depbudget))
            print("\n")
            
            print("Variable costs for department ", department)
            print("\n")        
            
            print("{:<20} {:<8} {:<15} {:<10} {:<10} {:<9} {:<15} {:<20}".format("ITEM", "QUANTITY", "COSTS", "DEPARTMENT", "USER", "DATE", "TARGET MONTH", "PURPOSE"))
               
            for row in GetData.get_rows(table, department, datum[:6]):
                id, item, quant, costs, department, user, date, targetmonth, purpose = row
                print("{:<20} {:<8} {:<15} {:<10} {:<10} {:<9} {:<15} {:<20}".format(item, quant, costs, department, user, date, targetmonth, purpose))
                
            
            befehl = "SELECT SUM(COSTS) FROM VARIABLE_COSTS WHERE TARGET_MONTH = " + '"' + datum[:6] + '"' + " AND DEPARTMENT = " + '"' + department + '"'
            my_cursor.execute(befehl)
            total = convert_list(my_cursor.fetchall())
            print("--------------------------------------------------------")    
            print("TOTAL: ", "%.2f" % total, "\n\n")
        except TypeError:
            print(listempty)
            print("There are no accounted costs for the department", department, "for the month", datum[:6], "at the moment.", sep=" ")
            print("\n")
        except:
            print("Error")
        
    elif table == "FIXED_COSTS":
        my_cursor.execute("SELECT * FROM PLAN WHERE MONTH = " + '"' + datum[:6] + '"')
        accrev = convert_list(my_cursor.fetchall())
        
        print(elem1)
        print("\nCurrent revenue: ", accrev[1])
        
        #Alle Budgets selektieren
        befehl = "SELECT * FROM BUDGET WHERE MONTH = " + '"' + datum[:6] + '"'
        my_cursor.execute(befehl)
        budgets = my_cursor.fetchall()
        
        try:
            print("\nBudgets for the month", datum[:6], sep=" ")
            for row in budgets:
                print("Budget of ", row[2], ": ", row[1])
                
            print("\n")
        except:
            print("Error: No budgets have been accounted for month ", datum[:6])
            
        print(elem1)

        try:
            befehl = "SELECT * FROM FIXED_COSTS"
            my_cursor.execute(befehl)
            fixcost = my_cursor.fetchall()
            
            print("Fixed costs: ")
            print("\n")
            print("{:<20} {:<4} {:<15} {:<10}".format("ITEM", "QUANTITY", "COSTS", "DATE"))

            for row in fixcost:
                id, item, quantity, costs, usr, date  = row
                print("{:<20} {:<8} {:<10} {:<15}".format(item, quantity, costs, date))
            print("--------------------------------------------------------")   
            befehl = "SELECT SUM(COSTS) FROM FIXED_COSTS"
            my_cursor.execute(befehl)
            total = convert_list(my_cursor.fetchall())    
            print("TOTAL: ", str("%.2f" % total), "\n\n")
        except:
            print("Error: No fixed costs for month", datum[:6], "have been found.", sep=" ")
        
def budget(month, planbudget, department):
    
    befehl = "SELECT SUM(AMOUNT) FROM BUDGET WHERE MONTH = " + '"' + month + '"'
    my_cursor.execute(befehl)
    budgetsum = convert_list(my_cursor.fetchall()) 
    
    my_cursor.execute("SELECT * FROM PLAN WHERE MONTH = " + '"' + month + '"')
    plans = convert_list(my_cursor.fetchall())
    
    my_cursor.execute("SELECT * FROM BUDGET WHERE MONTH = " + '"' + month + '"' + " AND DEPARTMENT = " + '"' + department + '"')
    bud = convert_list(my_cursor.fetchall())
    
    try:
        if month in bud:
            my_cursor.execute("UPDATE BUDGET SET AMOUNT = " + '"' + planbudget + '"' + " WHERE MONTH = " + '"' + month + '"' + " AND DEPARTMENT = " + '"' + department + '"')
            my_db.commit()
            
        else:
            if budgetsum == None:
                try:
                    if float(plans[2])-float(plans[4])>float(planbudget):
                        my_cursor.execute("INSERT INTO BUDGET (AMOUNT, DEPARTMENT, MONTH, EXPENDITURE) VALUES (" + planbudget + ', ' + '"' + department + '"' + ', ' + month + ', ' + '"' + "NOTYET" + '"' + ')')
                        my_db.commit()
                    else:
                        print("This portion of the over all budget will exceed the calculated revenue and can't be granted.")
                except TypeError:
                    print(typefail)
                except ValueError:
                    print("Revenues or Profits haven't been planned yet. Please do this first.")    
                    
            else: 
                try:
                    if float(plans[2])-float(plans[4])>float(budgetsum)+float(planbudget):
                        my_cursor.execute("INSERT INTO BUDGET (AMOUNT, DEPARTMENT, MONTH, EXPENDITURE) VALUES (" + planbudget + ', ' + '"' + department + '"' + ', ' + month + ', ' + '"' + str("notyet") + '"' + ')')
                        my_db.commit()
                        
                        print("The budget for the department", department, "takes ", str(float(planbudget)/float(plans[2][0])*100), "% of the planned revenue.")
                    else:
                        print("This portion of the over all budget will exceed the calculated revenue and can't be granted.")
                except TypeError:
                    print(typefail)
                except ValueError:
                    print("Revenues or Profits haven't been planned yet. Please do this first.")        
    except TypeError:
        print(typefail)
    except ValueError:
        print(valuefail)
    except:
        print("Error: ", errordetail)
    else:                
        print("The budget for", department, "has been updated with", planbudget, sep=" ")
        
var_kost = Kost()
fix_kost = Kost() 
###########################################################################################
##################################  USER LOG-IN ###########################################
###########################################################################################
time.sleep(1)

fetch_user = False

print("\n\n")

while fetch_user != True:
    uname = input("Please enter your username: ")
    password = input("Please enter your password: ")

    try:

        befehl = "SELECT * FROM USER WHERE NAME = " + '"' + str(uname) + '"' + " AND PASSWORD = " + '"' + str(password) + '"'
        my_cursor.execute(befehl)
        fetch_user = my_cursor.fetchall()
        
        if len(fetch_user) > 0:
            fetch_user = True
        else:
            fetch_user = False
        
        
        if fetch_user == True:
            print("Welcome, ", uname)
            befehl = "SELECT PRIVILEGES FROM USER WHERE NAME = " + '"' + str(uname) + '"'
            my_cursor.execute(befehl)
            privuser = convert_list(my_cursor.fetchall())

        else:
            print("Access denied")
            continue
        
    except:
        print("Error: A possible database error occured.\nPlease contact your administrator for checking the database")

print(elem1)
        
############################################################################################
##############################  HAUPTTEIL - Benutzer-Menu   ################################
############################################################################################
        
usertree = True

while usertree:
    privlist = []
    userbranch = input("What would you like to do? Type HELP to access the program help\n")
    

    if userbranch == "ENTRY":
        
        print(elem1)
        usevar = input("Would you like to enter (FIX)ed costs or (VAR)iable costs?\n")
        
        if usevar == "FIX":
            privlist = ["01",]
            if checkpriv(privuser, privlist) == False:
                print("Nescessary user privileges haven't been granted")
                continue
            
            print(elem1, "\n###################################  ENTERING FIXED COSTS    #########################################", elem1, "\n\n")
            fix_kost.enterfix(uname)
            
        
        elif usevar == "VAR": 
            dep = departments()
            
            befehl = "SELECT PRIVILEGES FROM DEPARTMENT WHERE NAME = " + '"' + str(dep) + '"'
            my_cursor.execute(befehl)
            privdep = convert_list(my_cursor.fetchall())
            
            privlist = ["01", privdep]
            if checkpriv(privuser, privlist) == False:
                print("Necessary user privileges haven't been granted")
                continue
            
            print(elem1, "\n#################################    ENTERING VARIABLE COSTS    #####################################", elem1,"\n\n")
            var_kost.entervar(privdep, uname)
            
        else:
            print("Sorry, " + usevar + "is no legal input")
        print(elem1, elem1)    

    elif userbranch == "CHART":
        
        print(elem1, "\n###################################    SCOOTEQ IN CHARTS    ######################################", elem1,"\n\n")
        
        Chart.Chart(input("Which month would you like to watch?\n"))
    
        print(elem1, elem1)
    
    elif userbranch == "PLAN":
        
        #Check privileges
        privlist = ["01", "02"]    
        if checkpriv(privuser, privlist) == False:
            print("Necessary user privileges haven't been granted")
            continue
        
        #Show Planning
        monthplan = input("Please enter the month(YYYYMM) to be watched: ")
        print("There is a revenue planned for " + monthplan + " about " + str(GetData.get_allmonth("PLAN", monthplan)[0][2]) + " and \na profit planned about " + str(GetData.get_allmonth("PLAN", monthplan)[0][4]))
        
        if input("Press Enter to continue, or LEAVE to get back to the main menu.\n") == "LEAVE":
            continue
        else:
            print(elem1, "\n#####################################  PLANNING FINANCES    ########################################", elem1, "\n\n")

            #Enter the planning
            category = input("Would you like to plan a future (REV)enue or (PROF)it?\n")
            enterin(category, userbranch)
            print("The planning has been updated. Thank You.")
        
        print(elem1, elem1)
        
    elif userbranch == "INCOME":
        
        #Check privileges
        privlist = ["01"]    
        if checkpriv(privuser, privlist) == False:
            print("Necessary user privileges haven't been granted")
            continue
        
        print(elem1, "\n#####################################  ACCOUNTING NEW REVENUE    ########################################", elem1, "\n\n")

        enterin("REV", userbranch)
        
        print(elem1, elem1)
        
    elif userbranch == "SHOW":
        deprt = privuser
        
        print(elem1, "\n######################################  SHOWING DATA   ########################################", elem1, "\n\n")

        
        #Zugang für Admin (Fixkosten und gewählte Abteilung)
        if privuser == "01":
            befehl = "SELECT PRIVILEGES FROM DEPARTMENT WHERE NAME =" + '"' + str(departments()) + '"'
            my_cursor.execute(befehl)
            selcdep = convert_list(my_cursor.fetchall())
            deprt = selcdep
            showdata("FIXED_COSTS", get_department(selcdep), convertdate(date.today()))
        
        #Zugang für Finance (Fixkosten und Finance)    
        elif privuser == "02":
            showdata("FIXED_COSTS", get_department(deprt), convertdate(date.today()))
        
        #Zugang für alle anderen (nur zugehörige Abteilung)
        showdata("VARIABLE_COSTS", get_department(deprt), convertdate(date.today()))
    
        print(elem1, elem1)
    
    elif userbranch == "BUDGET":
        
        #Check privileges
        privlist = ["02"]    
        if checkpriv(privuser, privlist) == False:
            print("Nescessary user privileges haven't been granted")
            continue
        
        print(elem1, "\n###################################  DISTRIBUTING BUDGETS   ######################################", elem1, "\n\n")
        #Enter Budgets            
        time = input("For which month will the budget be set? \n")
        print("At the moment available money for budgets:", str(float(GetData.totalmonth("PLAN", time, "REVENUE_PLANNED")) - float(GetData.totalmonth("BUDGET", time, "AMOUNT"))), sep=" ")
        print("\n")
        department = departments()
        print("\n")
        amount = input("What amount is set in the budget?\n")
        
        budget(time, amount, department)
    
        print(elem1, elem1)
        
    elif userbranch == "HELP":
        print("\n\n##################################################################################################\n###################################      PROGRAM HELP        ######################################\n##################################################################################################\n")
        print("Welcome to the program help of CALC-U-LATER\nYou can enter these commands to the console:")
        print("Enter 'ENTRY' to create a new entry to the fixed or variable costs.")
        print("Enter 'SHOW' to see accounted costs and items.")
        print("Enter 'INCOME' to book any revenue or profit.")
        print("Enter 'PLAN' to plan future revenue or proft.")
        print("Enter 'BUDGET' to plan the budget for departments.")
        print("Enter 'CHART' to show different charts of revenues, profit and costs.\n")
        print("Enter 'LEAVE' to leave the program.")
        print("Please consider that some functions can only be accessed by user with designated privileges.\nPlease enter departments in CAPITAL letters.")
        print(showdepartments())
        print(" Dates should be written this way: YYYYMMDD.\n'Target dates' should be written this way: YYYYMM.")
        print("##################################################################################################\n\n")
        
    elif userbranch == "LEAVE":
        usertree = False    
        print("##################################################################################################")
        print("##################################################################################################")
        print("BYE, BYE")  
    else:
        print("Sorry,", userbranch, "is no legal input", sep=" ")
        print("\n")

        
        
#OPEN POINTS:
#Testen/Debuggen
#budget
#Daten zu Grafiken erzeugen
#try-except zu allen Funktionen mit passenden Fehlermeldungen bauen (falsches Command im user tree)
#Grafische Oberfläche
#Weitere Datenbank-Daten schreiben