DROP TABLE IF EXISTS USER;
DROP TABLE IF EXISTS FIXED_COSTS;
DROP TABLE IF EXISTS DEPARTMENT;
DROP TABLE IF EXISTS VARIABLE_COSTS;
DROP TABLE IF EXISTS PLAN;
DROP TABLE IF EXISTS BUDGET;


CREATE TABLE DEPARTMENT (
                            ID INT NOT NULL AUTO_INCREMENT,
                            PRIVILEGES VARCHAR(255),
                            NAME VARCHAR (255),
                            PRIMARY KEY(ID)
);

CREATE TABLE USER (
                      ID int NOT NULL AUTO_INCREMENT,
                      DEPARTMENT_ID INT NOT NULL,
                      PASSWORD VARCHAR(255),
                      PRIVILEGES VARCHAR(255),
                      NAME VARCHAR(255),
                      PRIMARY KEY(ID),
                      FOREIGN KEY(DEPARTMENT_ID) REFERENCES DEPARTMENT(ID)
);

CREATE TABLE FIXED_COSTS (
                             ID int NOT NULL AUTO_INCREMENT,
                             ITEM VARCHAR(255),
                             QUANTITY INT NOT NULL ,
                             COSTS FLOAT,
                             USER VARCHAR(255),
                             DATE INT NOT NULL,
                             PRIMARY KEY(ID)
);


CREATE TABLE VARIABLE_COSTS (
                                ID INT NOT NULL AUTO_INCREMENT,
                                ITEM VARCHAR(255),
                                QUANTITY VARCHAR(255),
                                COSTS FLOAT,
                                DEPARTMENT VARCHAR(255),
                                USER VARCHAR(255),
                                DATE INT NOT NULL,
                                TARGET_MONTH INT NOT NULL,
                                PURPOSE VARCHAR(255),
                                PRIMARY KEY(ID)
);

CREATE TABLE BUDGET (
                        ID INT NOT NULL AUTO_INCREMENT,
                        AMOUNT FLOAT,
                        DEPARTMENT VARCHAR(255),
                        MONTH INT NOT NULL,
                        EXPENDITURE VARCHAR(255),
                        PRIMARY KEY(ID)
);

CREATE TABLE PLAN (
                      ID INT NOT NULL AUTO_INCREMENT,
                      REVENUE_ACTUAL FLOAT,
                      REVENUE_PLANNED FLOAT,
                      PROFIT_ACTUAL FLOAT,
                      PROFIT_PLANNED FLOAT,
                      MONTH INT NOT NULL,
                      PRIMARY KEY(ID)
);

INSERT INTO DEPARTMENT VALUES (1, "01", "ADMIN");
INSERT INTO DEPARTMENT VALUES (2, "02", "FINANCE");
INSERT INTO DEPARTMENT VALUES (3, "03", "IT");
INSERT INTO DEPARTMENT VALUES (4, "04", "DEVELOPMENT");
INSERT INTO DEPARTMENT VALUES (5, "05", "HR");
INSERT INTO DEPARTMENT VALUES (6, "06", "MANAGEMENT");
INSERT INTO DEPARTMENT VALUES (7, "07", "MARKETING");
INSERT INTO DEPARTMENT VALUES (8, "08", "DISTRIBUTION");


INSERT INTO USER VALUES (1, 1, "QWERTZ", "01", "john_cena");
INSERT INTO USER VALUES (2, 3, "ASDFGH", "03", "eren_jeager");
INSERT INTO USER VALUES (3, 2, "123456", "02", "erik_smith");
INSERT INTO USER VALUES (4, 1, "nobudget", "01", "mathis_cra");
INSERT INTO USER VALUES (5, 1, "giorgi", "01", "goirgi_maz");
INSERT INTO USER VALUES (6, 1, "derlehrer", "01", "heiko_mei");
INSERT INTO USER VALUES (7, 6,  "andererlehrer", "06", "niklas_ehr");
INSERT INTO USER VALUES (8, 4, "password", "04", "curt_angle");
INSERT INTO USER VALUES (9, 5, "ggggg", "05", "jeff_hardy");
INSERT INTO USER VALUES (10, 7, "pass", "07", "brock_lesnar");
INSERT INTO USER VALUES (11, 8, "word", "08", "halk_hogan");

INSERT INTO BUDGET VALUES (3, 40000, "IT", 202204, "new laptops");
INSERT INTO BUDGET VALUES (2, 60000, "HR", 202205, "very very important stuff");
INSERT INTO BUDGET VALUES (1, 50000, "FINANCE", 202206, "a lot of beer");
INSERT INTO BUDGET VALUES (4, 50500, "FINANCE", 202207, "keep it low");
INSERT INTO BUDGET VALUES (5, 55000, "IT", 202207, "more than last time");
INSERT INTO BUDGET VALUES (6, 50000, "DEVELOPMENT", 202207, "budget");
INSERT INTO BUDGET VALUES (7, 70000, "HR", 202207, "budget for HR");
INSERT INTO BUDGET VALUES (8, 33000, "MANAGEMENT", 202207, "budget");
INSERT INTO BUDGET VALUES (9, 54000, "MARKETING", 202207, "budget");
INSERT INTO BUDGET VALUES (10, 34000, "DISTRIBUTION", 202207, "budget");

INSERT INTO FIXED_COSTS VALUES (1, "MS Office Suite", 20, 2500, "mathis_cra", 20220627);
INSERT INTO FIXED_COSTS VALUES (2, "Rent", 1, 10000, "mathis_cra", 20220628);
INSERT INTO FIXED_COSTS VALUES (3, "Power", 1, 200, "giorgi_maz", 20220629);
INSERT INTO FIXED_COSTS VALUES (4, "Water", 1, 50, "john_cena", 20220629);
INSERT INTO FIXED_COSTS VALUES (5, "Wages", 50, 125000, "mathis_cra", 20220629);
INSERT INTO FIXED_COSTS VALUES (6, "somthing", 3, 300, "goirgi_maz", 20220701);
INSERT INTO FIXED_COSTS VALUES (7, "meal", 50, 3000, "heiko_mei", 20220702);
INSERT INTO FIXED_COSTS VALUES (8, "beverages", 25, 14500, "mathis_cra", 20220703);

INSERT INTO PLAN VALUES (1, 550000, 500000, 8000, 6000, 202205);
INSERT INTO PLAN VALUES (2, 650000, 649000, 7000, 9000, 202206);
INSERT INTO PLAN VALUES (3, 660000, 650000, 9500, 9900, 202207);
INSERT INTO PLAN VALUES (4, 660700, 660100, 9500, 10000, 202208);

INSERT INTO VARIABLE_COSTS VALUES (1, "PS5", 15, 8000, "IT", "eren_jeager", 20220629, 202207, "for work atmosphere");
INSERT INTO VARIABLE_COSTS VALUES (2, "mouse", 2, 1000, "FINANCE", "erik_smith", 20220626, 202207, "for work");
INSERT INTO VARIABLE_COSTS VALUES (3, "monitor", 13, 2000, "HR", "jeff_hardy", 20220627, 202207, "for new employees");
INSERT INTO VARIABLE_COSTS VALUES (4, "A4 paper", 5000, 800, "DEVELOPMENT", "curt_angle", 20220701, 202207, "The printer was empty");
INSERT INTO VARIABLE_COSTS VALUES (5, "Money", 10000, 10000, "MANAGEMENT", "niklas_ehr", 20220702, 202207, "We needed the money.");
INSERT INTO VARIABLE_COSTS VALUES (6, "alcohol", 50, 4000, "MARKETING", "brock_lesnar", 20220703, 202207, "for party");
INSERT INTO VARIABLE_COSTS VALUES (7, "dollars", 1000, 7000, "DISTRIBUTION", "halk_hogan", 20220704, 202207, "We need the money.");
INSERT INTO VARIABLE_COSTS VALUES (8, "laptop", 3, 6000, "IT", "eren_jeager", 20220629, 202208, "new laptops");
INSERT INTO VARIABLE_COSTS VALUES (9, "keyboards", 6, 200, "FINANCE", "erik_smith", 20220626, 202208, "for work");
INSERT INTO VARIABLE_COSTS VALUES (10, "wodka", 13, 2000, "HR", "jeff_hardy", 20220627, 202208, "for party");
INSERT INTO VARIABLE_COSTS VALUES (11, "something", 30, 2000, "DEVELOPMENT", "curt_angle", 20220701, 202208, "very important");
INSERT INTO VARIABLE_COSTS VALUES (12, "some stuff", 10, 3000, "MANAGEMENT", "niklas_ehr", 20220702, 202208, "we need it");
INSERT INTO VARIABLE_COSTS VALUES (13, "alcohol", 50, 6000, "MARKETING", "brock_lesnar", 20220703, 202208, "for party");
INSERT INTO VARIABLE_COSTS VALUES (14, "PS5 games", 23, 1000, "DISTRIBUTION", "halk_hogan", 20220704, 202208, "for chilling");