# LF02
Project for LF02 - Finance calculation program CALC-U-LATER

## Getting started
Before starting the app make sure, that you have MySQL installed. It is also mandatory to have an empty database. If not please create a new database and run the file create_tables.sql file. Simply copy all code there and run it on your database. After that you have all what you need to use our application. 
The application file is named index_bu.py. We recommend to run it in an editor where you can enlarge your console (best on full screen) to see everything happening on screen.

## Description
CALC-U-LATER was created for the ScootTech company, in order to control the budget of the company, variable and fixed costs and also to match planned and actual revenue and profit.  

## Usage
After starting the program you have to write your database set ups, which consist of host, user, password and database. Afterwards please use your user credentials to log in. We created special user accounts for you with which you can enter the program. The first user has administrator privileges. The user name is 'heiko_mei' and password is 'derlehrer'. The second account has the privileges for the management department. The user name you should use here is 'niklas_ehr' and the password is andererlehrer. We also would like to give you the account for all the finance privileges. Therefore you can log in as 'erik_smith' with the password '123456'. With these three accounts you should be able to get a good general overview of the different functionalities of CALC-U-LATER. 

## Unit tests
Having downloaded all this via git in this folder there should also be a file named test_index.py
Here you can see our unit tests to try them yourselves.

## Support
If you have any problem or question according this application you are free to contact with our developers team. Our contacts: giorgimazm@gmail.com and mathis.cramer@icloud.com 

## Authors and acknowledgment
This application was created by Mathis Cramer and Giorgi Mazmishvili for the third assignment of the Lernfeld 2 task. 

## Project status
The project is not completely done, there are still some fitchers that could be released in future, for example - GUI, we have started with it, but it is not yet finished. You can how we started with GUI in special GUI branch on GitLab. 
