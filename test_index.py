from datetime import date
import unittest
import index

class Test_index(unittest.TestCase): 
  
  def test_checkpriv_true(self):
    userpriv = [1] 
    privlist = [1, 2, 3]
    result = index.checkpriv(userpriv, privlist)
    self.assertEqual(result, True)


  def test_checkpriv_false(self):
    userpriv = [0] 
    privlist = [1, 2, 3]
    result = index.checkpriv(userpriv, privlist)
    self.assertEqual(result, False)


if __name__ == '__main__': 
    unittest.main() 